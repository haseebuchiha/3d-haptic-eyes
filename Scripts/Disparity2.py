#import cv2 
#import numpy as np

#def blockGenerator(img, blocksize):
#    for row in range(0, 1275):
#        for col in range(0, 955):
#            pixel = img[row,col]
#            #print (pixel)`
#            for rowI in range(0, blocksize):
#                for colI in range(0, blocksize):
#                    val1 = row + rowI
#                    val2 = col + colI
#                    #For iterating in the block this is the code
#                    #block = img[val1, val2]
#                    #img[val1, val2] = 255
#                    #print("row " )
#                    #print(val1)
#                    #print("col ")
#                    #print(val2)
#                    #print("block complete")
                    
#            #    if colI == 0 or colI == blocksize -1:
#            #        img[val1, val2] = 255
#            #        #//img[val1, val2] = pixel
#            #if (rowI == 0 or rowI == blocksize -1):
#            #    img[val1, val2] = 255
#            #    #img[val1, val2] = pixel
#            col += blocksize
#        row += blocksize
#    cv2.namedWindow('img', cv2.WINDOW_NORMAL)
#    cv2.resizeWindow('img', 480, 640)

#    cv2.imshow('img', img)
#    cv2.waitKey(0)
#    cv2.destroyAllWindows()
#    #cv2.imwrite(img)

    #Experimental


#def Similarity(image, image2, blocksize):
#    #get to a specified point in the picture
#    #for row in range(0, 1275):
#        #for col in range(0, 955):
#    pixel = image[100:105, 100:105]
#    pixel2 = image2[100:105, 100:105]
#    pixel3 = image2[102:107, 102:107]
#    pixel4 = image2[105:110, 105:110]
#    sim = pixel - pixel2
#    sim2 = pixel - pixel3
#    sim3 = pixel - pixel4
    
#    print('Sim1: ', sim)
#    print('Sim2: ', sim2)
#    print('Sim3: ', sim3)
#    similar = sim - sim2
#    similar2 = sim - sim3
#    print ("Similarity: ", similar)
#    print ("Similarity2: ", similar2)
#    #if similar < similar2:
#    #    print(True)
#    #else:
#    #    print(False)
            
#    #take the pixels from up, down, left and right

#image = cv2.imread("img1.jpeg")
#image2 = cv2.imread("img2.jpeg")
#Similarity(image, image2, 5)

import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread("rey.jpg", 0)

height, width= img.shape
#cropping scene
imgL = img[0 : height, 0 : int(width/2)]
imgR = img[0 : height, int(width/2) : width]

stereo = cv2.StereoBM_create(numDisparities=64, blockSize=15)
disparity = stereo.compute(imgR,imgR)
plt.imshow(disparity,'gray')
plt.show()