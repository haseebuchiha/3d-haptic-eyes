import cv2
import os
import numpy as np


img = cv2.imread("Images_for_laserpoint_detection\IMG_Dist21.jpg", -1)
cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.resizeWindow('img', 600, 600)

cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
cv2.resizeWindow('mask', 600, 600)

cv2.namedWindow('res', cv2.WINDOW_NORMAL)
cv2.resizeWindow('res', 600, 600)


Hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# define range of red color in HSV
lower_red = np.array([0, 0, 210])
upper_red = np.array([60, 150, 255])

# Threshold the HSV image to get only red colors
mask = cv2.inRange(Hsv, lower_red, upper_red)

# Bitwise-AND mask and original image
res = cv2.bitwise_and(img, img, mask=mask)
cv2.imshow('img', img)
cv2.imshow('mask', mask)
cv2.imshow('res', res)

cv2.waitKey(0)
