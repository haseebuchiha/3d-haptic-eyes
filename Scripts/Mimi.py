import cv2
import numpy as np
import os

# Playing video from file:
cap = cv2.VideoCapture('test3.mp4')

try:
    if not os.path.exists('data'):
        os.makedirs('data')
except OSError:
    print('Error: Creating directory of data')

currentFrame = 0
ret, frame = cap.read()
frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
cv2.rotate(frame, cv2.ROTATE_180, frame)
im, im2 = frame, frame
cv2.WINDOW_AUTOSIZE
while(True):
    # Capture frame-by-frame
    im = im2
    ret, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    im2 = frame
    cv2.rotate(frame, cv2.ROTATE_180, frame)
    frame = im - im2
    retval2, threshold2 = cv2.threshold(
        frame, 150, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    cv2.imshow('frame', threshold2)
    cv2.waitKey(0)
    if not ret:  # if no ret then break the loop
        break

    # Saves image of the current frame in jpg file
    name = './data/frame' + str(currentFrame) + '.jpg'
    print('Creating...' + name)
    cv2.imwrite(name, frame)
    # To stop duplicate images
    currentFrame += 1

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
