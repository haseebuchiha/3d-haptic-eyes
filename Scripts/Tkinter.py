import tkinter as tk
import PIL
from PIL import Image, ImageTk
import cv2
import compare_pix as cp
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import logging
logging.getLogger('matplotlib').disabled = True
from matplotlib.axes._axes import _log as matplotlib_axes_logger
matplotlib_axes_logger.setLevel('ERROR')




def depthEstimation():
    depth = []
    dispval = []

    img = cv2.imread('wut1.jpg')
    img1 = cv2.imread('wut2.jpg')

    h, w, c = img.shape

    dispmap = cp.disparityMap(h, w)
    depthmap = cp.depthMap(h, w)
    edgepic = cp.edgeDetect(img)


    for row in range(0 + 7, h - 7 ):
        for col in range(0 + 200  ,w - 10 ):
            if edgepic[row, col] == 255:
                disp, img, img1 = cp.WeightedBlockGenerator(img, img, 5, w, h, row, col, 10, False, (0))
                dispmap[row, col] = disp
                dispval.append(disp)
                if disp < 10:
                    continue
                depthmap[row, col] = cp.DepthCalcGraph(disp)  # printing in milimeters
    cv2.imwrite('ggez.jpg', depthmap)
    depth1 = cv2.imread('ggez.jpg',0)
    cp.Interpolate_Image(depth1 , h, w)


window = tk.Tk()

window.title("3D Scanner")
# siz=800*600,position=(500,200)
window["bg"] = "gray1"
# icon
#self.icon = ImageTk.PhotoImage(file='hsv_icon.ico')
#self.window.call('wm', 'iconphoto', self.window._w, self.icon)

# NEED 4 frames

w=550
h=300
borderw = 5
bgFrame = 'gray26'
bgLabel = 'gray10'
paddingx = 2
paddingy = 5


window.columnconfigure([0,1], weight=1, minsize=100)
window.rowconfigure([0,1], weight=1, minsize=50)
window.bind('<Escape>', lambda e: window.quit())



cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)

cap2 = cap
#cap2 = cv2.VideoCapture(0)
cap2.set(cv2.CAP_PROP_FRAME_WIDTH, w)
cap2.set(cv2.CAP_PROP_FRAME_HEIGHT, h)

if not (cap.isOpened()):
    cap = cv2.VideoCapture(2)
    print("Could not open video device")

cam1 = tk.Frame(master=window, width=w, height=h, borderwidth=borderw, bg=bgFrame, padx= paddingx, pady=paddingy, highlightthickness=1)
cam1.grid(row=0, column=0, padx= paddingx, pady=paddingy)
cam_rec = tk.Label(master=cam1, width=math.floor(w/8), height=math.floor(h/16), bg=bgLabel)
cam_rec.columnconfigure(0, minsize=200)
cam_rec.rowconfigure(0, minsize=100)
cam_rec.pack()


cam2 = tk.Frame(master=window, width=w, height=h, borderwidth=borderw, bg=bgFrame, padx= paddingx, pady=paddingy, highlightthickness=1)
cam2.grid(row=0, column=1, padx= paddingx, pady=paddingy)
cam_rec2 = tk.Label(master=cam2, width=math.floor(w/8), height=math.floor(h/16), bg=bgLabel)
cam_rec2.columnconfigure(0, minsize=200)
cam_rec2.rowconfigure(0, minsize=100)
cam_rec2.pack()

#def stop_cams():
#    global stop
#    stop=True

stop = True

def toggle_cam():
    global stop
    if stop:
        stop = False
    else:
        stop = True


def show_frame():
    _, frame = cap.read()
    if stop:
        cv2.imwrite('wut1.jpg', frame)
        cv2.imwrite('wut2.jpg', frame2)
        return
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = PIL.Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    cam_rec.imgtk = imgtk
    cam_rec.configure(image=imgtk, width=w, height=h)
    cam_rec.after(5, show_frame2)

def show_frame2():
    global frame2
    _, frame2 = cap2.read()
    frame2 = cv2.flip(frame2, 1)
    cv2image = cv2.cvtColor(frame2, cv2.COLOR_BGR2RGBA)
    img = PIL.Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    cam_rec2.imgtk = imgtk
    cam_rec2.configure(image=imgtk, width=w, height=h)
    cam_rec2.after(5, show_frame)


camera = tk.Button(
    text="Camera",
    width=20,
    height=3,
    bg="DimGrey",
    fg="White",
    command= lambda: [toggle_cam(), show_frame()]
)
camera.grid(row=2, column=0, padx=20, pady=5, sticky='ws')



#Depth map
depth_frame = tk.Frame(master=window, width=w, height=h, borderwidth = borderw, bg=bgFrame, padx= paddingx, pady=paddingy, highlightthickness=1)
depth_frame.grid(row=1, column=0, padx= paddingx, pady=paddingy)
depth_result = tk.Label(master=depth_frame, width=math.floor(w/8), height=math.floor(h/16), bg=bgLabel)
depth_result.columnconfigure(0, minsize=200)
depth_result.rowconfigure(0, minsize=100)
depth_result.pack()
def show_depth():
    frame = cv2.imread("Interggez.jpg")
    #frame = cv2.imread('Depth.jgp')
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = PIL.Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    depth_result.imgtk = imgtk
    depth_result.configure(image=imgtk, width=w, height=h)


depth_btn = tk.Button(
    text="Depth",
    width=20,
    height=3,
    bg="DimGrey",
    fg="White",
    #command=show_depth
    command= lambda: [depthEstimation(), show_depth()]
)

depth_btn.grid(row=2, column=0, padx=20, pady=5, sticky='s')



#3D plot
plot_3D = tk.Frame(master=window, width=w, height=h, borderwidth = borderw,padx= paddingx, pady=paddingy, bg=bgFrame, highlightthickness=1)
plot_3D.grid(row=1, column=1, padx= paddingx, pady=paddingy)
plotter = tk.Label(master=plot_3D, width=math.floor(w/8), height=math.floor(h/16), bg=bgLabel)
plotter.columnconfigure(0, minsize=200)
plotter.rowconfigure(0, minsize=100)
plotter.pack()

def plot_depth():
    image = cv2.imread('Interggez.jpg',0)
    height, width = image.shape
    #image = image[:, :, ::-1]
    #depth_map = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    depth_map = image
    image = cv2.resize(image, (width // 20, height // 20), cv2.INTER_AREA)
    height, width = image.shape
    depth_map = cv2.resize(depth_map, (width, height), cv2.INTER_AREA)
    fig = plt.figure()
    ax3D = Axes3D(fig)
    for x in range(0, height-1):
        #print("Processing row %i/%i." % (x, height))
        for y in range(0, width-1):
            z = float(depth_map[x, y])
            #color = image[x, y] / 255.
            ax3D.scatter(x, y, z, s = 12, c = 'b' , marker='x')
            #c=None for mixed colors 
    plt.show()

def save_depth_csv():
    image = cv2.imread('Interggez.jpg',0)
    height, width = image.shape
    depth_map = image
    image = cv2.resize(image, (width // 20, height // 20), cv2.INTER_AREA)
    height, width = image.shape
    depth_map = cv2.resize(depth_map, (width, height), cv2.INTER_AREA)
    # save the ploting depth map as csv
    np.savetxt('depth_plot.csv', image, delimiter=',', fmt='%d')


def load_depth_csv():
    #read the saved csv as image
    file = open("depth_plot.csv")
    image_plot = np.loadtxt(file, delimiter=",")
    height, width = image_plot.shape
    fig = plt.figure()
    ax3D = Axes3D(fig)
    for x in range(0, height-1):
        #print("Processing row %i/%i." % (x, height))
        for y in range(0, width-1):
            z = float(image_plot[x, y])
            #color = image[x, y] / 255.
            ax3D.scatter(x, y, z, s = 12, c = 'b' , marker='x')
            #c=None for mixed colors
    plt.show()

plot_btn = tk.Button(
    text="Plot",
    width=20,
    height=3,
    bg="DimGrey",
    fg="White",
    command=plot_depth
)

plot_btn.grid(row=2, column=1, padx=20, pady=5, sticky='s')

save_csv_btn = tk.Button(
    text="save depthmap",
    width=20,
    height=3,
    bg="DimGrey",
    fg="White",
    command=save_depth_csv
)

load_csv_btn = tk.Button(
    text="load depthmap",
    width=20,
    height=3,
    bg="DimGrey",
    fg="White",
    command=load_depth_csv
)
save_csv_btn.grid(row=2, column=1, padx=20, pady=5, sticky='w')


load_csv_btn.grid(row=2, column=1, padx=20, pady=5, sticky='e')


window.mainloop()
