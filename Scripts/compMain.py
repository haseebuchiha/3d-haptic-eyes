##Rules for commit

##Always fetch and pull before changing file.
##Search 'TODO' to see current problem that need to be fixed.
##Always comment out the current problem that has not been fixed before commit starting with 'TODO'.
##Search 'NEW' to see new assigned task.
##Remove or add 'TODO' or 'New' once the task is complete/incomplete

import cv2

import compare_pix as cp

#imgL = [
#    cv2.imread('./data/Test Images/Nsignalc2r.jpg'), 
#    cv2.imread('./data/Test Images/Nsignalc2.5r.jpg'),
#    cv2.imread('./data/Test Images/Nsignalc3r.jpg'),
#    cv2.imread('./data/Test Images/Nsignalc3.5r.jpg')
#]

#TimgL = [
#    cv2.imread('./data/Test Images/TiltLNsignalc2.5r.jpg'),
#    cv2.imread('./data/Test Images/TiltRNsignalc2.5r.jpg'),
#    cv2.imread('./data/Test Images/TiltLNsignalc3.5r.jpg'),
#    cv2.imread('./data/Test Images/TiltRNsignalc3.5r.jpg')
#]

imgL = [
    cv2.imread('./data/Mimi Data/images test data/WaterCooler1L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler2L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler3L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler4L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler5L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler6L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler7L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler8L.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler9L.jpg')

]

imgR = [
    cv2.imread('./data/Mimi Data/images test data/WaterCooler1R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler2R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler3R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler4R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler5R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler6R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler7R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler8R.jpg'),
    cv2.imread('./data/Mimi Data/images test data/WaterCooler9R.jpg')
]


#imgL = [
#    cv2.imread('./data/Mimi Data/4L2.jpg'), 
#    cv2.imread('./data/Mimi Data/4.6L2.jpg'),
#    cv2.imread('./data/Mimi Data/5L2.jpg'),
#    cv2.imread('./data/Mimi Data/5.6L2.jpg'),
#    cv2.imread('./data/Mimi Data/6L2.jpg'),
#    cv2.imread('./data/Mimi Data/6L3.jpg')
#]


#imgR = [
#    cv2.imread('./data/Mimi Data/4R2.jpg'), 
#    cv2.imread('./data/Mimi Data/4.6R2.jpg'),
#    cv2.imread('./data/Mimi Data/5R2.jpg'),
#    cv2.imread('./data/Mimi Data/5.6R2.jpg'),
#    cv2.imread('./data/Mimi Data/6R2.jpg'),
#    cv2.imread('./data/Mimi Data/6R3.jpg')
#]

#for i in range(0 , 1):
#    #cv2.imshow('ImageL', imgL[i])
#    #cv2.imshow('ImageR', imgR[i])
#    #cv2.waitKey(0)

#    h, w, c = imgL[i].shape
#    edges = cp.edgeDetect(imgR[i])
#    cp.disparityTester(imgL[i], imgR[i], w, h, edges)
index = 4

#TODO: Union of image issue. Sub with original with box and add with new box;
h, w, c = imgL[index].shape

edges = cp.edgeDetect(imgL[index])  
#edges[:, :] = 255
##print( edges.shape)
#cv2.imshow("edge",edges)
##print( imgL[5].shape)

###swapped left and right due to naming error in old files not in images test data  or images
#cp.TesterByMimi(edges, imgL[1], imgR[1] , w , h)
#cp.disparityTester(imgL[i], imgR[i], w, h, edges)

###Subtracts the blocks and returns graph values.

###imgDots1 = cp.dotGen(imgL, 150, 20)
###imgDots2 = cp.dotGen(imgR, 150, 20)

dispmap = cp.disparityMap(h, w)

depthmap = cp.depthMap(h, w)
####cp.calcDepth(dispmap) 
## for img in range(0,3):
#edges = cp.edgeDetect(imgL[3])
## cv2.imshow("edges", edges)
## cv2.waitKey(0)
## cv2.destroyAllWindows()
####################################################################################
## checking the Disparity Points

## cp.disparityFinder(imgL[0], imgR[0], w, h, edges)
##cp.some(imgL[3], imgR[3], w, h, edges)
#print(cp.ComputeDepthv2(58, 443, 68))

###################################################################################

#################################################################################
# Depth Estimation
#depth1 = cv2.imread('Depth.jpg',0)
#print(depth1.shape)
#cp.Interpolate_Image(depth1 , h, w)
#cv2.waitKey(0)
cp.depthEstimation(imgL[index], imgR[index], w, h, edges, dispmap, depthmap)

###################################################################################
# Calculating depth from points stored in an image
# TODO: take data from file instead of picture
# depthimg = cv2.imread("Depth.jpg")
# points = []
##Make a function to calculate the std for a depth map
#points = cp.stdAvgDepth(depthimg)

# print("average: ", np.average(points))
# print("std: ", np.std(points))
 
# f = plt.figure()
# plt.imshow(points)
# plt.show()


#############################################################3

# capture images realtime by pressing space
#cp.captureImages()
