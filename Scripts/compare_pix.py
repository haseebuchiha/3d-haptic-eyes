# Rules for commit

# Always fetch and pull before changing file.
# Search 'TODO' to see current problem that need to be fixed.
# Always comment out the current problem that has not been fixed before commit starting with 'TODO'.
# Search 'NEW' to see new assigned task.
# Remove or add 'TODO' or 'New' once the task is complete/incomplete

import cv2
import numpy as np
import math
import os
import matplotlib.pyplot as plt
import random
from mpl_toolkits import mplot3d
import compare_pix as cp

try:
    if not os.path.exists('./data/Difference'):
        os.makedirs('./data/Difference')

except OSError:
    print("No direcory in data named Difference found, Creating directory")
try:
    if not os.path.exists('./data/Depth'):
        os.makedirs('./data/Depth')

except OSError:
    print("No direcory in data named Difference found, Creating directory")


#TEST
def no(num):
    print(num)


def TesterByMimi(edge, imgL, imgR , width , height):
#third addition is to skip usless points
    num = 0
    color = 255
    for i in range(0 + 7 + 200 , height + 7 - 100 , 10 ):
        for j in range(0 + 200  ,width - 100 , 10 ):
            if edge[i,j] == 255:
                disp, img, img1 = WeightedBlockGenerator(imgL, imgR, 5, width, height, i, j, 10, False , color  , num)
                num = num + 1
                color = color - 15
                print (disp)
                cv2.imshow("imgL",imgL)
                cv2.imshow("imgR",imgR)
#                if cv2.waitKey(33) == ord('a'):
                cv2.waitKey(0)


def WeightedBlockGenerator(img1, img2, blocksize, width, height, ref_row=0, ref_col=0, scan_dist=0, color=False, C=0,
                           num=0):
    # if not color:
    #    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    #    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    #img1 = img3
    #img2 = img4
    if scan_dist == 0:
        scan_dist = ref_row

    max_bound = math.floor(blocksize / 2)
    # Doing Maxbound + 1 because the : operator of nparray ignores the last value
    block1 = img1[ref_row - max_bound: ref_row + max_bound + 1, ref_col - max_bound: ref_col + max_bound + 1]
    block1 = block1.astype(np.int32)

    #print( block1[1:4, 1])
    block1[1:4, 1] = block1[1:4, 1] * 64
    block1[1:4, 3] = block1[1:4, 3] * 64
    block1[1, 2] = block1[1, 2] * 64
    block1[3, 2] = block1[3, 2] * 64
    block1[2, 2] = block1[2, 2] * 128
    darr = []

    prevsum = 99999999
    c_col = ref_col
    c_row = ref_row

    ##Show scan area
    #    for row in range(ref_row -100), (ref_row + 100):
    #        for col in range((ref_col - 100) , (ref_col + 100) ):
    #            img2[ref_row , col] = 0

    # The number of rows are not measures here and the issue is that the L images point is lower than the Right one
    # for row in range(ref_row - scan_dist +max_bound , ref_row + scan_dist - max_bound):

    # column was taken as same reference point because the point should be near this point in the second image
    # TODO: Changed the area of find from 0 to 50 instead of -5, +5
    for row in range(ref_row -5 , ref_row + 5):
        # for col in range( max_bound, (width  - max_bound)):
        for col in range(ref_col - 190, ref_col):
            block2 = img2[row - max_bound: row + max_bound + 1, col - max_bound: col + max_bound + 1]
            # changing block type
            block2 = block2.astype(np.int32)
            block2[1:4, 1] = block1[1:4, 1] * 64
            block2[1:4, 3] = block1[1:4, 3] * 64
            block2[1, 2] = block1[1, 2] * 64
            block2[3, 2] = block1[3, 2] * 64
            block2[2, 2] = block1[2, 2] * 128
            dblock = block1 - block2
            dblock = np.absolute(dblock)
            dsum = np.sum(dblock)
            # Do not take the sum of dblock so that you can see the answer array with values
            # aray[[[168]]]
            if dsum < prevsum:
                prevsum = dsum
                # offset = math.sqrt(math.pow((ref_col - col),2)  + math.pow((ref_row - row),2))   #calculating the hypotenues using sqrt(A^2 + B^2)
                offset = np.absolute(ref_col - col)
                # TODO: Add offset for rows as well.
                c_col = col
                c_row = row
            elif dsum == prevsum:
                # cur_offset = math.sqrt(math.pow((ref_row - col),2)  + math.pow((ref_col - row),2))
                cur_offset = np.absolute(ref_col - col)
                if offset > cur_offset:
                    c_col = col
                    c_row = row
                    offset = cur_offset
                    # making it so that if 2 points are equal then the point thats closest to source point should be choosen
                    # print("found another point")
            darr.append(dsum)
    # print("RimgCol: ", c_col)
    # print("RimgRow: ", c_row)
    # print(min(darr))
    # print("DISP: ", ref_col - c_col)
    # cv2.circle(img1,(ref_row, ref_col), 3, C,10)
    # cv2.circle(img2,(ref_row,c_col), 8, C,10)
    dot_size = max_bound + 5
    # print(ref_row , c_col)
    # print(img1[ref_row , ref_col])
    # print(img2[ref_row , c_col])

    ## First image
    ## img1[ref_row - dot_size: ref_row + dot_size + 1, ref_col - dot_size: ref_col + dot_size + 1] = C
    #start_point = (ref_col - dot_size, ref_row - dot_size)
    #end_point = (ref_col + dot_size + 1, ref_row + dot_size + 1)
    ## Create hollow rentangle

    ## adding Text to the image
    #font = cv2.FONT_HERSHEY_SIMPLEX
    
    #image = cv2.rectangle(img1, start_point, end_point, (C, C + 5, C + 10), 2)
    ##image = cv2.putText(image, str(num), end_point, font, 1, (0, 0, 0), 2, cv2.LINE_AA)

    ## second image
    ## img2[ref_row - max_bound : ref_row + max_bound , c_col - max_bound : c_col + max_bound] = C
    ## img2[ref_row - dot_size: ref_row + dot_size + 1, c_col - dot_size: c_col + dot_size + 1] = C
    #res_start_point = (c_col - dot_size, ref_row - dot_size)
    #res_end_point = (c_col + dot_size + 1, ref_row + dot_size + 1)
    #image1 = cv2.rectangle(img2, res_start_point, res_end_point, (C, C + 5, C + 10), 2)
    ##image1 = cv2.putText(image1, str(num), res_end_point, font, 1, (0, 0, 0), 2, cv2.LINE_AA)

    return ref_col - c_col, img1, img2


def blockGenerator(img1, img2, blocksize, width, height, ref_row=0, ref_col=0, scan_dist=0, color=False, C=0):
    if not color:
        img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    if scan_dist == 0:
        scan_dist = ref_row

    max_bound = math.floor(blocksize / 2)

    block1 = img1[ref_row - max_bound: ref_row + max_bound + 1, ref_col - max_bound: ref_col + max_bound + 1]
    block1 = block1.astype(np.int32)

    darr = []

    prevsum = 1000000
    c_col = ref_col
    c_row = ref_row

    ##Show scan area
    #    for row in range(ref_row -100), (ref_row + 100):
    #        for col in range((ref_col - 100) , (ref_col + 100) ):
    #            img2[ref_row , col] = 0

    # The number of rows are not measures here and the issue is that the L images point is lower than the Right one
    # for row in range(ref_row - scan_dist +max_bound , ref_row + scan_dist - max_bound):

    # column was taken as same reference point because the point should be near this point in the second image
    for row in range(ref_row - 50, ref_row + 50):
        # for col in range( max_bound, (width  - max_bound)):
        for col in range(ref_col - 170, ref_col):
            block2 = img2[row - max_bound: row + max_bound, col - max_bound: col + max_bound]
            # changing block type
            block2 = block2.astype(np.int32)
            dblock = block1 - block2
            dblock = np.absolute(dblock)
            dsum = np.sum(dblock)
            # Do not take the sum of dblock so that you can see the answer array with values
            # aray[[[168]]]
            if dsum < prevsum:
                prevsum = dsum
                # offset = math.sqrt(math.pow((ref_col - col),2)  + math.pow((ref_row - row),2))   #calculating the hypotenues using sqrt(A^2 + B^2)
                offset = np.absolute(ref_col - col)
                c_col = col
                c_row = row
            elif dsum == prevsum:
                # cur_offset = math.sqrt(math.pow((ref_row - col),2)  + math.pow((ref_col - row),2))
                cur_offset = np.absolute(ref_col - col)
                if offset > cur_offset:
                    c_col = col
                    c_row = row
                    offset = cur_offset
                    # making it so that if 2 points are equal then the point thats closest to source point should be choosen
                    # print("found another point")

            darr.append(dsum)
    # print("RimgCol: ", c_col)
    # print("RimgRow: ", c_row)
    # print(min(darr))
    # print("DISP: ", ref_col - c_col)
    # cv2.circle(img1,(ref_row, ref_col), 3, C,10)
    # cv2.circle(img2,(ref_row,c_col), 8, C,10)
    dot_size = max_bound + 5
    # print(ref_row , c_col)
    # print(img1[ref_row , ref_col])
    # print(img2[ref_row , c_col])
    img1[ref_row - dot_size: ref_row + dot_size, ref_col - dot_size: ref_col + dot_size] = C
    # img2[ref_row - max_bound : ref_row + max_bound , c_col - max_bound : c_col + max_bound] = C
    img2[ref_row - dot_size: ref_row + dot_size, c_col - dot_size: c_col + dot_size] = C

    return ref_col - c_col, img1, img2

    # plotting graph for values
    # for i in range(0, len(darr)):
    #    val = int(darr[i-1]/16)
    #    val = ref_point-val
    #    if val<0:
    #        val = 0
    #    if color == True:
    #        img2[val:val+10,i+(ref_point - scan_dist)] = (255,0,0)
    #    else:
    #        img2[val:val+10,i+(ref_point - scan_dist)] = (0)

    # Analysis states that the drawing of the circle is probably wrong So,
    # We have to see how to circle is being drawn

    ##Creating circles for similarities of images
    # cv2.circle(img1,(ref_col, ref_row), 4, (0,0,255), 10)
    # cv2.imshow("src_image", img1)

    ##The circle that it being drawn at the index is not the cordinate point of the image
    # point = min(darr)
    # cv2.circle(img2,(c_col, c_row), 4, (0,0,255),10 )

    # cv2.imshow("dst_image", img2)
    # TODO: Make random values for various results.
    # cv2.imwrite('./data/Difference/15.jpeg', img1)
    # cv2.imwrite('./data/Difference/16.jpeg', img2)

    # cv2.waitKey(1)
    # function ends


# TODO: Delete after use
def exp_blockfinder(img1, img2, blocksize, h, w):
    block1 = img1[100: 100 + blocksize, 100: 100 + blocksize]
    block1 = block1.astype(np.int32)

    darr = []
    max_bound = math.floor(blocksize / 2)

    prevsum = 1000000
    p_col = 0

    for row in range(0 + math.floor(blocksize / 2), h - math.floor(blocksize / 2)):
        for col in range(0 + math.floor(blocksize / 2), w - math.floor(blocksize / 2)):
            block2 = img2[row - max_bound: row + max_bound, col - max_bound: col - max_bound + blocksize]
            # changing block type
            block2 = block2.astype(np.int32)
            dblock = block1 - block2
            dblock = np.absolute(dblock)
            dsum = np.sum(dblock)

            if dsum == 0:
                print("kill yourself lol")

            if dsum < prevsum:
                prevsum = dsum
                p_col = col

            darr.append(dsum)
    print(min(darr))
    print(p_col)


# TODO: Delete after use
def dotGen(img, somepoint, space_bet):
    # for col in range(somepoint, somepoint+100, space_bet):
    #    img[100 : 100+5, col : col+5] = (0,0,255)

    # cv2.imshow("src_image", img)
    # cv2.waitKey(1)
    for row in range(somepoint, somepoint + 100, space_bet):
        for col in range(somepoint, somepoint + 100, space_bet):  # 400, 400 + 100, 20
            img[row: row + 3, col: col + 3] = (0, 0, 255)
    nimg = img
    # cv2.imshow("src_image", nimg)
    # cv2.imwrite('nimg1.jpg', nimg)

    cv2.waitKey(1)
    return nimg


# Create depth map of the image
# TODO: Refactor due
# old iMPLIMENTATION
def ComputeDepth(img1, img2, blocksize, width, height, threshhold, step):
    darr = []
    some_array = []
    max_bound = math.floor(blocksize / 2)

    row = 0
    col = 0
    mini = 10000
    maxi = 0

    # Create an image with all values set as zero with similar dimensions to source image (i.e. the width and height of the images are same)
    blank_img = np.zeros((height, width, 3), np.uint8)

    # Compute disparity as before but this time for every row and every column
    print("computing disp.....")
    prev_index = 0
    cur_index = 0
    dep_Point = 0
    dep_Point2 = 0
    dep_Col = 0
    dep_Col2 = 0
    min = 0
    black_count = 0
    # First box the current first box should be the current column

    for row in range(0 + max_bound, height - blocksize):

        for col in range(blocksize + max_bound, width - blocksize):
            dsum2 = 10000000
            block1 = img1[row: row + blocksize, col: col + blocksize]
            block1 = block1.astype(np.int32)
            for coli in range((col - blocksize * 5), col):
                block2 = img2[row: row + blocksize, coli: coli + blocksize]
                # changing block type
                block2 = block2.astype(np.int32)
                dblock = block1 - block2
                dblock = np.absolute(dblock)
                dsum1 = np.sum(dblock)
                darr.append(dsum1)
                if dsum1 < dsum2:
                    # second_dispPoint = dsum1
                    dsum2 = dsum1
                    min = coli

            dist = col - min
            if dist < 0:
                dist = (dist * (-1))
                # Thresholding the results
            # if mini > dist:
            #    mini = dist
            # if maxi < dist:
            #    maxi = dist
            # for getting max and mix vals in the disparity
            # gimme the second box
            # Gimme the distance between them and then consider it done
            # Create the depth effect using the formula
            # Formula for depth is focal length times baseline divided by distance/disparity

            #:  Normalization the results

            focal_length = 0.4 / 100  # m = 0.004
            baseline = 2 / 100  # m 0.02
            depth = (((focal_length * baseline) / dist) - 0.0000032) * 3320312.5  # ((.004 * .02) / 1...25)
            # Writing the Depth to the image
            blank_img[row, col] = depth
            # So the values are 0.0000032 ... 0.00008
            # and 0 ... 0.0000768
            # so divide 255 by 0.0000768 
            # Hence  3320312.5 is the number we need to multiply for normalization

            ## :Interpolation
            # if depth < 100:

            #    if black_count == 0:
            #        row_count = row
            #        dep_Col = col
            #    black_count = black_count + 1
            # else:
            #    #taking first point
            #    if black_count == 0:
            #       dep_Point = blank_img[row, col]
            #    else:
            #        if row_count == row:
            #            #taking second point
            #            dep_Point2 =  blank_img[row, col]
            #            dep_Col2 = col
            #            Inc = abs(dep_Point - dep_Point2)/black_count

            #            # subtracting inc if first point is bigger
            #            print(type(dep_Point) ,type(dep_Point2))
            #            if(dep_Point > dep_Point2.all()):
            #                for x in range(dep_Col , dep_Col2):
            #                    blank_img[row, x] = blank_img[row, x-1] - Inc

            #            else:
            #            #adding inc if first point is smaller
            #                for x in range(dep_Col , dep_Col2):
            #                    blank_img[row, x] = blank_img[row, x-1] + Inc
            #            black_count = 0
            #        else:
            #            black_count = 0

    # print( str(mini)+"....."+ str(maxi))
    cv2.imshow('dst_image', blank_img)
    cv2.imwrite('./data/Depth/%d' % random.randint(1, 10000) + '.jpeg', blank_img)


# Function end

def Interpolate_Image(img , h , w):
    img2 = img
    for row in range(0 , h):
        nWhitePixel = 0
        firstPoint = 0
        secondPoint = 0
        for col in range(0,w):
            if(img[row,col] != 0):
                if(firstPoint == 0):
                    firstPoint = col
                elif(secondPoint == 0):
                    secondPoint = col
                    if(nWhitePixel !=0 ):
                        increment = (secondPoint - firstPoint)/nWhitePixel 
                        for index in range(col - nWhitePixel ,col):
                            img[row,index] = firstPoint + (increment * (index - col - nWhitePixel )) 

                    firstPoint = secondPoint
                    secondPoint = 0
                    nWhitePixel = 0

            elif(firstPoint != 0):
                nWhitePixel +=1
    cv2.imwrite('Interggez.jpg', img)
    #cv2.imshow('juice', img)
    #f = plt.figure()
    #f.add_subplot(1, 2, 1)
    ## f.add_subplot(1, 2, 1)
    #plt.imshow(img2)
    #f.add_subplot(1, 2, 2)
    #plt.imshow(img)
    ## plt.figure()
    ###plt.plot(dispval, depth)
    #plt.show(block=True)



# TODO: Delete after use
def finding_rows(img1, img2):
    # Draw sqares on rows only
    for row in range(0, 640):
        cv2.circle(img2, (row, 100), 40, (0, 0, 255), 10)

    # a = img1[194, 300] = (0,0,255)
    # b = img2[201, 252] = (0,0,255)

    # a = (0,0,255)
    # b = (0,0,255)

    cv2.imshow("src_image", img1)

    # End result it is column rows not rows column

    # The circle that it being drawn at the index is not the cordinate point of the image
    # point = min(darr)
    # cv2.circle(img2,(c_row, c_col), 40, (0,0,255),10 )

    cv2.imshow("dst_image", img2)
    cv2.imwrite('./data/Difference/15.jpeg', img1)
    cv2.imwrite('./data/Difference/16.jpeg', img2)

    cv2.waitKey(1)


# TODO: Delete after use
def finding_cols(img1, img2):
    # Draw circle on cols only
    # for col in range(cur_col, h):
    #     cv2.circle(img2, (200, col), 40, (0, 0, 255), 10)

    cv2.imshow("src_image", img1)

    # The circle that it being drawn at the index is not the cordinate point of the image
    # point = min(darr)
    # cv2.circle(img2,(c_row, c_col), 40, (0,0,255),10 )

    cv2.imshow("dst_image", img2)
    cv2.imwrite('./data/Difference/15.jpeg', img1)
    cv2.imwrite('./data/Difference/16.jpeg', img2)

    cv2.waitKey(1)


# New: All function are now available in compare_pix.py and the implementation is in compMain.py
# TODO: Real time image check using dual cams.

# TODO: Convert image into binary, take a point from the image and find that point in a gray image(Please confirm).
def imgtobin(img, min, max, type):
    if type == "BINARY":
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, nimg = cv2.threshold(gray, min, max, cv2.THRESH_BINARY)
        # cv2.imshow("BINARY", nimg)
        return gray, nimg
    elif type == "BINARY_INV":
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, nimg = cv2.threshold(gray, min, max, cv2.THRESH_BINARY_INV)
        # cv2.imshow("BINARY_INV", nimg)
        return gray, nimg
    elif type == "TRUNC":
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, nimg = cv2.threshold(gray, min, max, cv2.THRESH_TRUNC)
        # cv2.imshow("TRUNC", nimg)
        return gray, nimg


# TODO: Find row and col point where the image is white
def findwhitepoint(bimg, h, w, ref_h, ref_w):
    for row in range(ref_h, h):
        for col in range(ref_w, w):

            if bimg[row, col] == 0:

                print(row - ref_h)

            elif bimg[row, col] == 255:
                # print("wPointCol: ", col)
                # print("wPointrow: ", row)
                return row, col


def edgeDetect(img):
    edges = cv2.Canny(img, 70, 255)
    cv2.imwrite('./data/Difference/edges.jpeg', edges)
    # cv2.imshow('imgedge1', edges)
    # cv2.waitKey(0)

    # TODO: Eliminate any white points out of area of search and clear the image

    return edges
    # cv2.imshow("img", edges)
    # cv2.waitKey(1)


def disparityMap(height, width):
    img = np.zeros((height, width), np.uint16)
    img = img + 255
    return img
    # cv2.imshow("img", img)
    # cv2.waitKey(1)
    # 304.8mm is 1ft


def depthMap(height, width):
    img = np.zeros((height, width), np.uint16)
    #img[:, :] = 255
    return img


def focalLengthFinder(b, disp, z):
    f = (disp * z) / b
    print("FocalLength: ", f)
    return f


def baseLineFinder(f, disp, z):
    b = disp * z / f
    print(b)


def ComputeDepthv2(disp, f, b):
    # z= (155)/disp
    z = f * b / disp
    # print(z)
    return z

def DepthCalcGraph(disp):
    # z= (155)/disp
    depth = 1339 + -22.2*disp + 0.146*disp**2 + -0.000337*disp**3
    # print(z)
    return depth

def stdAvgDepth(someimg):
    h, w, c = someimg.shape
    calimg = np.zeros((h, w, 3), np.uint16)
    calimg[:, :] = 2000
    points = []
    for row in range(10, h):
        for col in range(10, w):
            # someimg[row, col] = 0
            # if np.all(someimg[row, col] == 0) or np.all(someimg[row,col] >= 150):
            #     continue

            # if np.all(someimg[row,col] == 255):
            #    continue
            if (np.all(someimg[row, col] <= 100) and np.all(someimg[row, col] > 10)):
                # print(someimg[row,col])
                points.append(someimg[row, col])
    return points


def depthEstimation(imgL, imgR, w, h, edgepic, dispmap, depthmap):
    #focalLength = [
    #    466.4705882,  # 2ft
    #    484.412,  # 2ft TEST
    #    426.1029412,  # 2.5ft
    #    336.3970588,  # 3ft
    #    282.5735294  # 3.5ft
    #]

    depth = []
    dispval = []

    # TODO: Create filing
    #f = open("depthdata.txt", "a+")
    #k = open("dispdata.txt", "a+")
    for row in range(0 + 7, h - 7 ):
        for col in range(0 + 200  ,w - 10 ):
            if edgepic[row, col] == 255:
                
                disp, img, img1 = WeightedBlockGenerator(imgL, imgR, 5, w, h, row, col, 10, False, (0))
                dispmap[row, col] = disp
                dispval.append(disp)
                #k.write(str(disp) + "\n")
                if disp < 10:
                    continue
                # print("disp: ", disp)
                # if(i<4):
                #    #Calculate focal length
                #    if(disp < 40):
                #       f.append(focalLengthFinder(68, disp, 305))
                #    i = i+1
                # avgf = np.average(f)
                # f = focalLengthFinder(68,disp,305)
                depthmap[row, col] = DepthCalcGraph(disp)  # printing in milimeters
                #print(DepthCalcGraph(disp))
                # TODO: depth equals that value of z
                # depth.append(ComputeDepthv2(disp, focalLength[0], 68))
                # FOR WRITING DATA
                # f.write(str(depth) + "\n")

                # print("depth: ", depth)
                # print(1)
                # conversion from 524 pixels to 149mm
                # https://www.pixelto.net/px-to-mm-converter
                # print("depth: ", ComputeDepthv2(disp, 159, 68))
    #f.close()
    #k.close()

    # find average and find standard deviation

    # FOR READING DATA
    # f = open("depthdata", "r")
    # fdata = f.read()
    # stddisp = np.std(disp)
    # print("Standard deviation depth: ", stddepth)
    # print("Standard deviation disp: ", stddisp)
    # print("average depth: ", avgdepth)
    # print("average disp: ", avgdisp)

    #cv2.imwrite('disparity.jpg', dispmap)
    cv2.imwrite('newTest.jpg', depthmap)
    
    #x = np.linspace(0,639,30)

    #y = np.linspace(0,479,30)

    #X,Y = np.meshgrid(x,y)

    #Z = F(depthmap , X,Y)

    #fig = plt.figure()
    #ax = plt.axes(projection = '3d')
    #ax.contour3D(X , Y , Z , 50 , cmap= 'binary')
    #ax.set_xlabel('x')
    #ax.set_ylabel('y')
    #ax.set_zlabel('z')
    #plt.show(block=True)

    #cv2.waitKey(0)
    #f = plt.figure()
    #f.add_subplot(1, 2, 1)
    #plt.imshow(dispmap)
    #f.add_subplot(1, 2, 2)
    #plt.imshow(depthmap)
    #plt.show(block=True)

def F(depthMap,x,y):
    return depthMap[x,y]


def some(imgL, imgR, w, h, edges):
    img = imgL
    img1 = imgR
    color = 0
    num = 0
    disparity = []
    avgdepth = []
    for row in range(128, 273, 20):
        for col in range(277, 505, 50):
            row_w, col_w = findwhitepoint(edges, h, w, row, col)
            if col_w > 635:
                continue
            num = num + 1
            disp, img, img1 = WeightedBlockGenerator(img, img1, 5, w, h, row_w, col_w, 10, False, color, num)
            # depthmap[row, col] = (ComputeDepthv2(disp, 484.412, 68)) #printing in milimeters
            avgdepth.append(ComputeDepthv2(disp, 418.814338235294, 68))
            if color > 180:
                color = 0
            else:
                color = color + 50
            print("disp: ", disp)
            disparity.append(disp)
            f = plt.figure()
            f.add_subplot(1, 2, 1)
            plt.imshow(img)
            f.add_subplot(1, 2, 2)
            plt.imshow(img1)
            plt.show(block=True)
    print(np.average(disparity))
    print(np.average(avgdepth))


def disparityTester(imgL, imgR, w, h, edges):
    img = imgL
    img1 = imgR
    color = 0
    num = 0

    for i in range(80, h - 80):
        for j in range(200 , w - 200 ):
            num = num + 1
            if(edges[i,j] == 255 ):
                
                disp, img, img1 = WeightedBlockGenerator(img, img1, 5, w, h, i, j, 10, False, color, num)
                if color > 180: 
                    color = 0
                else:
                    color = color + 50

                if(num > w*h - 500):
                    #print("disp: ", disp)
                    f = plt.figure()
                    f.add_subplot(1, 2, 1)
                    plt.imshow(img)
                    f.add_subplot(1, 2, 2)
                    plt.imshow(img1)
                    
                    plt.show(block=True)



def disparityFinder(imgL, imgR, w, h, edges):
    img = imgL
    img1 = imgR
    color = 0
    num = 0
    for i in range(10, 1000):
        if i % 30 == 0:
            row_w, col_w = findwhitepoint(edges, h, w, i, 500)

            if col_w > 635:
                continue
            num = num + 1
            disp, img, img1 = WeightedBlockGenerator(img, img1, 5, w, h, row_w, col_w, 10, False, color, num)
            # depthmap[row, col] = (ComputeDepthv2(disp, 484.412, 68)) #printing in milimeters
            print((ComputeDepthv2(disp, 484.412, 68)))
            if color > 180:
                color = 0
            else:
                color = color + 50
            print("disp: ", disp)
            f = plt.figure()
            f.add_subplot(1, 2, 1)
            plt.imshow(img)
            f.add_subplot(1, 2, 2)
            plt.imshow(img1)
            plt.show(block=True)

    # 1ft:
    #     disp:  128
    #     ref_col:  266
    #     cur_col:  138
    # print(cp.focalLengthFinder(68, 128, (1 * 305)))
    #    FocalLength:  430.5882352941176

    # 2ft:
    #    disp:  63
    #    ref_col:  301
    #    cur_col:  238
    # print(cp.focalLengthFinder(68, 63, (2*305)))
    #    FocalLength:  565.1470588235294

    # 3ft 430 focal length
    #    disp:  43
    #    ref_col:  325
    #    cur_col:  282
    # print(cp.focalLengthFinder(68, 43, (3*305)))
    #    FocalLength:  578.6029411764706

    # avg of all 3 images
    # 524.3333333333334
    # avg = (430+565+578)/3
    # print(avg)


def captureImages():
    # capture images realtime by pressing space
    cap = cv2.VideoCapture(1)
    cap1 = cv2.VideoCapture(2)
    # cap2 = cv2.VideoCapture(0)
    while (True):
        # Capture frame-by-frame
        ret, frameL = cap.read()
        ret, frameR = cap1.read()
        # ret, frame2 = cap2.read()
        # Our operations on the frame come here
        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        #frame1 = cv2.rotate(frame1, cv2.ROTATE_90_CLOCKWISE)

        # Display the resulting frame
        # edges = cp.edgeDetect(frame)
        cv2.imshow('frameL', frameL)
        cv2.imshow('frameR', frameR)
        # cv2.imshow('frame3',frame2)

        # if cv2.waitKey(1) & 0xFF == ord('c'):
        #    cv2.imwrite("./data/Test Images/SIGNALl.jpg", frame1)
        #    cv2.imwrite("./data/Test Images/SIGNALr.jpg", frame)

        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #    break
        k = cv2.waitKey(1)
        # if k==27:    # Esc key to stop
        #    break
        if k == 32:
            cv2.imwrite("./data/Mimi Data/images test data/Dol150L.jpg", frameL)
            cv2.imwrite("./data/Mimi Data/images test data/Dol150R.jpg", frameR)
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    cv2.waitKey(0)
    cv2.destroyAllWindows()



def captureImagesWeb():
    # capture images realtime by pressing space
    cap = cv2.VideoCapture(0)
    while (True):
        # Capture frame-by-frame
        ret, frameL = cap.read()
        # ret, frame2 = cap2.read()
        # Our operations on the frame come here
        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        #frame1 = cv2.rotate(frame1, cv2.ROTATE_90_CLOCKWISE)

        # Display the resulting frame
        # edges = cp.edgeDetect(frame)
        try:
            cv2.imshow('frameL', frameL)
        except:
            print("No camera detected")
            return
        # cv2.imshow('frame3',frame2)

        # if cv2.waitKey(1) & 0xFF == ord('c'):
        #    cv2.imwrite("./data/Test Images/SIGNALl.jpg", frame1)
        #    cv2.imwrite("./data/Test Images/SIGNALr.jpg", frame)

        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #    break
        k = cv2.waitKey(1)
        # if k==27:    # Esc key to stop
        #    break
        if k == 32:
            cv2.imwrite("./data/Mimi Data/images test data/Dol150L.jpg", frameL)
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    cv2.waitKey(0)
    cv2.destroyAllWindows()
