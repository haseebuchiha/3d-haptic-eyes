import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

# Example image
image_file = cbook.get_sample_data('grace_hopper.png')
image = plt.imread(image_file)

(r, c, b) = np.shape(image)

# X and Y coordinates of points in the image, spaced by 10.
(X, Y) = np.meshgrid(range(0, c, 10), range(0, r, 10))
# Display the image
plt.imshow(image)
# Plot points from the image.
plt.scatter(X, Y, image[Y,X])
plt.show()