import cv2
import numpy as np
from sklearn.preprocessing import normalize
import os
import pandas as pd
from pyntcloud import PyntCloud
from PIL import Image

try:
    if not os.path.exists('./data/Disparity'):
        os.makedirs('./data/Disparity')

except OSError:
    print("No direcory in data named Difference found, Creating directory")

#imgL = cv2.imread('img1.jpeg')
#imgR = cv2.imread('img2.jpeg')
def disp():
    
    img = cv2.imread("Lion.jpg")

    height, width, c = img.shape
    #cropping scene
    imgL = img[0 : height, 0 : int(width/2)]
    cv2.imwrite("seperated.jpg", imgL)
    imgR = img[0 : height, int(width/2) : width]

    #imgL = cv2.imread("lion1.jeg")
    #imgR = cv2.imread("lion2.jpg")

    window_size = 5

    left_matcher = cv2.StereoSGBM_create(minDisparity = 0, numDisparities = 160, blockSize = 5, P1 = 8*3 *window_size **2, P2 = 32 * 3 * window_size **2, disp12MaxDiff = 1, uniquenessRatio = 15, speckleWindowSize = 0, speckleRange = 2, preFilterCap = 63, mode = cv2.STEREO_SGBM_MODE_SGBM_3WAY)

    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)

    lmbda = 80000
    sigma = 1.2
    visualMultiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left = left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    print('computing Disparity')

    displ = left_matcher.compute(imgL, imgR)
    dispr = right_matcher.compute(imgR, imgL)

    filteredImg = wls_filter.filter(displ, imgL, None, dispr)

    filteredImg = cv2.normalize(src = filteredImg, dst = filteredImg, beta = 0, alpha = 255, norm_type = cv2.NORM_MINMAX)
    filteredImg = np.uint8(filteredImg)
    cv2.namedWindow('Disparity Map', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Disparity Map', 800, 800)
    cv2.imshow('Disparity Map', filteredImg)
    #cv2.imwrite('./data/Difference/16.jpeg', img2)
    cv2.imwrite('DISP0.JPEG', filteredImg)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


#Creating depth
def depthing():
    colourImg = Image.open("seperated.jpg")
    colourPixels = colourImg.convert("RGB")
    
    colourArray = np.array(colourPixels.getdata()).reshape((colourImg.height, colourImg.width) + (3,))
    indicesArray = np.moveaxis(np.indices((colourImg.height, colourImg.width)), 0, 2)
    imageArray = np.dstack((indicesArray, colourArray)).reshape(-1, 5)
    df = pd.DataFrame(imageArray, columns = ["x", "y", "red", "green", "blue"])

    depthImg = Image.open("DISP0.jpeg").convert('L')
    depthArray = np.array(depthImg.getdata())
    df.insert(loc = 2, column='z', value = depthArray)
    
    df[['x', 'y', 'z']] = df[['x', 'y', 'z']].astype(float)
    df[['red', 'green', 'blue']] = df [['red', 'green', 'blue']].astype(np.uint)
    
    #optional
    df['z'] = df['z']*5
    
    cloud = PyntCloud(df)
    #cloud.plot()
    cloud.to_file("lion.ply", also_save = ["mesh", "points"], as_text = True)
disp()
depthing() 