import cv2
import os
#import numpy as np

cap = cv2.VideoCapture('test1.mp4')
success, image = cap.read()
cv2.rotate(image, cv2.ROTATE_180, image)
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)


try:
    if not os.path.exists('data'):
        os.makedirs('data')
        #useless comment

except OSError:
    print("No directory name data found, creating directory")

try:
    if not os.path.exists('./data/Sub1'):
        os.makedirs('./data/Sub1')

except OSError:
    print("No direcory in data named sub0 found, Creating directory")


count = 0

cv2.imwrite('./data/Sub0/subframe.png', thresh)


def subTo1(currentFrame):
 # Stracting a single image with all of the frames
    subImage = cv2.imread('./data/Sub0/subframe.png')
    subImage -= currentFrame
    cv2.imwrite('./data/Sub0/subImage%d' % count + '.png', subImage)


def subToEachOther(currentFrame, count):
    # Subtracting each new image with the previous image
    if count == 0:
        prevFrame = cv2.imread('./data/frame' + str(count) + '.png')
    else:
        prevFrame = cv2.imread('./data/frame' + str(count-1) + '.png')
        prevFrame -= currentFrame
    cv2.imwrite('./data/Sub1/subToEach' + str(count) + '.png', prevFrame)


while(success):
    name = './data/frame' + str(count) + '.png'

    cv2.imwrite(name, thresh)
    # Image is in color but is converted for subtraction
    currentFrame = cv2.imread(name)

    subTo1(currentFrame)
    subToEachOther(currentFrame, count)

    success, image = cap.read()
    cv2.rotate(image, cv2.ROTATE_180, image)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)

    print("reading new frame: %d" % count, success)

    count += 1
    if not success:
        break

cap.release()
cv2.destroyAllWindows()
