
import cv2
import numpy as np 

def nothing (x):
    pass
def Trackbaring():
    
    img = np.zeros((300,512,3), np.uint8)
    cv2.namedWindow('image')

    cv2.createTrackbar('R', 'image', 0, 255, nothing)

    switch = '0 : OFF \n1 : ON'
    cv2.createTrackbar(switch, 'image', 0, 1,nothing)

    while(1):
        cv2.imshow('image', img)

        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        r = cv2.getTrackbarPos('R', 'image')
        g = cv2.getTrackbarPos('G', 'image')
        b = cv2.getTrackbarPos('B', 'image')
        s = cv2.getTrackbarPos('switch', 'image')

        if s==0:
            img[:] = 0
        else:
            img[:] = [b,g,r]

    cv2.destroyAllWindows()

#individual values in the array 
def iteming():
    img = cv2.imread('img1.jpeg')
    print(img.item(3))

def sizing():
    img = cv2.imread('img1.jpeg')
    print(img.size)

def blending():
    img = cv2.imread('img1.jpeg')
    img2 = cv2.imread('img2.jpeg')

    dst =cv2.addWeighted(img, 0.7, img2, 0.3, 0)
    cv2.imshow("dst", dst)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def croping():
    img = cv2.imread("wpic.jpg")

    height, width, c = img.shape
    #cropping scene
    imgL = img[0 : height, 0 : int(width/2)]
    imgR = img[0 : height, int(width/2) : width]

    #Succ

    cv2.imshow("bruh", imgL)
    cv2.imshow("bruh2", imgR)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def contour():
    img = cv2.imread("img1.jpeg")
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 127,255,0)
    image, contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[4]
    img = cv2.drawContours(img, [cnt], 0, (0,255,0), 3)

def Moment():
    img = cv2.imread("img1.jpeg", 0)
    ret, thresh = cv2.threshold(img, 127,255,0)
    contours, hierarchy = cv2.findContours(thresh, 1,2)

    cnt = contours[0]
    M = cv2.moments(cnt)
    print(M)

def test():
    img = cv2.imread('img1.jpeg')
    
    img[100:120, 100:120] = (255,0,0)
    cv2.imshow('some', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def jani():
    img = cv2.imread("img1.jpeg")
    for x in range(0, 1000):
        img[x, 0: 100] = (60, 79, 10)
    cv2.imshow("img", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def columncheck():
    img = cv2.imread("img1.jpeg")
    disp_sum = img[100, 100]
    print(disp_sum)            
columncheck()